﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CircleEnemy : MonoBehaviour
{
	
	[SerializeField] private Vector3 pos;
	[SerializeField] private float value;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
	private void FixedUpdate() {
		
		pos = transform.position;
		value += Time.fixedDeltaTime;
		
		float sin = Mathf.Sin(4 * value);
		pos.y = sin;
		
		transform.position = pos;
		
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
