﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//should I include math


public class Coin : Collectible
{
	
	
	[SerializeField] public GameObject player;
	[SerializeField] private float Distance;
	[SerializeField] private Vector3 distance2;
	
	//will home towards the player when the player comes closer
	//so we need to check vector distance and make the coin home towards the player
	
    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
	{
    	
		if (player == null) {
			
			return;
			//check null reference for player GameObject
		}
		
		
		Distance = CalculateDistance(player);
		
		if (Distance < 5.0f) {
			HomeAtPlayer(player);
		}
		
	    
    }
    
    
	private float CalculateDistance(GameObject p) {
		
		Vector2 distance = p.transform.position - transform.position;
		float sqrDistance;
		
		Debug.Log("Distance in Square Magnitude is: " + distance.sqrMagnitude);
		
		sqrDistance = distance.sqrMagnitude;
		
		return sqrDistance;
		
		
		
	}
    
    
	private void HomeAtPlayer(GameObject p) {
		
		Vector2 direction = p.transform.position - transform.position; //getting the direction to the player
		
		transform.Translate(direction.normalized * 3f * Time.deltaTime); //moving the gameObject towards the player
		
	}
	
	
	private void OnTriggerEnter2D(Collider2D other2D) {
		
		if (other2D.tag == "Player") //if the other collider is of tag Player
			Destroy(gameObject, 0.1f); //destroys the gameObject in 100 milliseconds
		
		
	}
	
    
}
