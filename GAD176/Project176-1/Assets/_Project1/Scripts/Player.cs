﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	
	//public private and other variables that player uses
	//could be speed
	// and rigidbody
	//also need to take account of the walls on the scene
	//
	
	[SerializeField] float moveSpeed = 10f;
	[SerializeField] public Rigidbody2D _rb;
	[SerializeField] int coinCount;
	
	private float moveX;
	
	
    // Start is called before the first frame update
    void Start()
	{
    	
		coinCount = 0;
	    
	    _rb = GetComponent<Rigidbody2D>();
	    
    }

    // Update is called once per frame
    void Update()
	{
		
		if (_rb == null) {
	    	
			Debug.Log("Warning: Player has no RigidBody attached to it");
			return;
	    	
		}
    	
		moveX = Input.GetAxis("Horizontal") * moveSpeed;
        
        
	    
		Vector2 velocity = _rb.velocity;
		velocity.x = moveX;
		_rb.velocity = velocity;
        
	}
    
    
	private void OnTriggerEnter2D(Collider2D other) {
		
		if(other.tag == "Coin") {
		
			coinCount++;
			Debug.Log("Coin Collected");	
		
		}
	
	}
    
    
}
